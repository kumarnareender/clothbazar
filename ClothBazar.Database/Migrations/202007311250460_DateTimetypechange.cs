﻿namespace ClothBazar.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimetypechange : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "CreatedOn", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "CreatedOn", c => c.DateTime(nullable: false));
        }
    }
}
