﻿namespace ClothBazar.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimeAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "CreatedOn", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "CreatedOn");
        }
    }
}
