﻿using ClothBazar.Database;
using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Services
{
    public class ConfigurationsService
    {

        #region Singleton
        public static ConfigurationsService Instance
        {
            get
            {
                if (instance == null) instance = new ConfigurationsService();

                return instance;
            }
        }
        private static ConfigurationsService instance { get; set; }
        private ConfigurationsService()
        {

        }
        #endregion

        public Config GetConfig(string Key)
        {
            using(var context = new CBContext())
            {
                return context.Configurations.Find(Key);
            }
        }

        public Config GetConfiguration(string Key)
        {
            using (var context = new CBContext())
            {
                return context.Configurations.Find(Key);
            }
        }


        public List<Config> GetConfigurations()
        {
            using (var context = new CBContext())
            {
                return context.Configurations.ToList();
            }
        }

        public void SaveConfiguration(Config configuration)
        {
            using (var context = new CBContext())
            {
                context.Configurations.Add(configuration);
                context.SaveChanges();
            }
        }

        public void UpdateConfiguration(Config configuration)
        {
            using (var context = new CBContext())
            {
                context.Entry(configuration).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }


        public void DeleteConfiguration(string Key)
        {

            using (var context = new CBContext())
            {
                var configuration = context.Configurations.Find(Key);
                //Two ways
                //1. context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                if (configuration != null)
                {
                    context.Configurations.Remove(configuration);

                    context.SaveChanges();
                }

            }
        }

    }
}
