﻿using ClothBazar.Database;
using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClothBazar.Services
{
    public class ProductsService
    {

        #region Singleton
        public static ProductsService Instance
        {
            get
            {
                if (instance == null) instance = new ProductsService();

                return instance;
            }
        }
        private static ProductsService instance { get; set; }
        private ProductsService()
        {

        }
        #endregion



        public Product GetProduct(int ID)
        {
            using (var context = new CBContext())
            {
                return context.Products.Where(x => x.ID == ID).Include(x => x.Category).FirstOrDefault();
            }
        }

        public List<Product> GetCartProducts(List<int> IDs)
        {
            using (var context = new CBContext())
            {
                return context.Products.Where(product=>IDs.Contains(product.ID)).ToList();
            }
        }
        public List<Product> GetProducts()
        {
            using (var context = new CBContext())
            {
                return context.Products.Include(x => x.Category).ToList();
            }
        }
        public void SaveProduct(Product product)
        {
            using (var context = new CBContext())
            {
                context.Entry(product.Category).State = System.Data.Entity.EntityState.Unchanged;
                context.Products.Add(product);
                context.SaveChanges();
            }
        }

        public void UpdateProduct(Product product)
        {
            using (var context = new CBContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteProduct(int ID)
        {

            using (var context = new CBContext())
            {
                var product = context.Products.Find(ID);
                //Two ways
                //1. context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                context.Products.Remove(product);
                context.SaveChanges();
            }
        }



        public int GetProductsCount(string search)
        {
            using (var context = new CBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Products
                        .Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower())).Count();
                }
                else
                {
                    return context.Products.Count();
                }
                //return context.Categories.Include(x => x.Products).ToList();
            }
        }



        public List<Product> GetProducts(string search, int pageNo)
        {
            int pageSize = int.Parse(ConfigurationsService.Instance.GetConfiguration("ListingPageSize").Value);
            using (var context = new CBContext())
            {
                //return context.Categories.Include(x => x.Products).ToList();

                if (!string.IsNullOrEmpty(search))
                {
                    return context.Products
                        .Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower()))
                        .OrderBy(x => x.ID)
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();
                }
                else
                {
                    return context.Products
                    .OrderBy(x => x.ID).Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

                }

            }
        }




    }
}
