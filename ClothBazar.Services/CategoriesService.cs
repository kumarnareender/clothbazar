﻿using ClothBazar.Database;
using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Services
{
    public class CategoriesService
    {

        #region Singleton
        public static CategoriesService Instance
        {
            get
            {
                if (instance == null) instance = new CategoriesService();

                return instance;
            }
        }
        private static CategoriesService instance { get; set; }
        private CategoriesService()
        {

        }
        #endregion

        public Category GetCategory(int ID)
        {
            using (var context = new CBContext())
            {
                return context.Categories.Where(x => x.ID == ID).FirstOrDefault();
            }
        }
        public int GetCategoriesCount(string search)
        {
            using (var context = new CBContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Categories
                        .Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower())).Count();
                }
                else
                {
                    return context.Categories.Count();
                }
                //return context.Categories.Include(x => x.Products).ToList();
            }
        }
        public List<Category> GetCategories(string search, int pageNo)
        {
            int pageSize = int.Parse(ConfigurationsService.Instance.GetConfiguration("ListingPageSize").Value);
            using (var context = new CBContext())
            {
                //return context.Categories.Include(x => x.Products).ToList();

                    if (!string.IsNullOrEmpty(search))
                    {
                        return context.Categories
                            .Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower()))
                            .OrderBy(x => x.ID)
                            .Skip((pageNo - 1) * pageSize)
                            .Take(pageSize)
                            .ToList();
                    }
                    else
                    {
                        return context.Categories
                        .OrderBy(x => x.ID).Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

                    }

                }
        }
        public List<Category> GetAllCategories()
        {
            using (var context = new CBContext())
            {
                //return context.Categories.Include(x => x.Products).ToList();

                return context.Categories.ToList();
                

            }
        }

        public List<Category> GetFeaturedCategories()
        {
            using (var context = new CBContext())
            {
                return context.Categories.Where(x=>x.IsFeatured && x.ImageURL != null).ToList();
            }
        }
        public void SaveCategory(Category category)
        {
            using (var context = new CBContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }

        public void UpdateCategory(Category category)
        {
            using (var context = new CBContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteCategory(int ID)
        {

            using (var context = new CBContext())
            {
                var category = context.Categories.Find(ID);
                //Two ways
                //1. context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                if(category != null)
                {
                    context.Categories.Remove(category);

                    context.SaveChanges();
                }
                
            }
        }
    }
}
