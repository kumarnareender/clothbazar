﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ClothBazar.Entities
{
    public class BaseEntity
    {
        [Key]

        public int ID { get; set; }

        [Required]
        [MinLength(5),MaxLength(50)]
        public string Name { get; set; }
        
        [MaxLength(500)]
        public string Description { get; set; }
        public string ImageURL { get; set; }
    }
}
