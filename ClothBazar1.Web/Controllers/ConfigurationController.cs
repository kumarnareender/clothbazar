﻿using ClothBazar.Entities;
using ClothBazar.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar1.Web.Controllers
{
    public class ConfigurationController : Controller
    {

        //ConfigurationsService configurationsService = new ConfigurationsService();

        // GET: Configuration
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ConfigurationTable(string search)
        {

            var configurations = ConfigurationsService.Instance.GetConfigurations();
            if (!string.IsNullOrEmpty(search))
            {

                configurations = configurations.Where(p => p.Key != null && p.Key.ToLower().Contains(search.ToLower())).ToList();
            }
            return PartialView(configurations);
        }

        //Create
        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }


        [HttpPost]
        public ActionResult Create(Config configuration)
        {
            ConfigurationsService.Instance.SaveConfiguration(configuration);
            return RedirectToAction("ConfigurationTable");
        }


        //Edit
        [HttpGet]
        public ActionResult Edit(string key)
        {
            var configuration = ConfigurationsService.Instance.GetConfiguration(key);
            return PartialView(configuration);
        }


        [HttpPost]
        public ActionResult Edit(Config configuration)
        {
            ConfigurationsService.Instance.UpdateConfiguration(configuration);
            return RedirectToAction("ConfigurationTable");
        }



        [HttpPost]
        public ActionResult Delete(string Key)
        {
            ConfigurationsService.Instance.DeleteConfiguration(Key);
            return RedirectToAction("ConfigutaionTable");
        }
    }
}