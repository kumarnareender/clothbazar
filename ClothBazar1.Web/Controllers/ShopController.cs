﻿using ClothBazar.Services;
using ClothBazar1.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar1.Web.Controllers
{
    public class ShopController : Controller
    {

        CheckoutViewModel model = new CheckoutViewModel();
        //ProductsService productsService = new ProductsService();


        // GET: Shop


        public ActionResult Checkout()
        {
            var CartProductsCookie = Request.Cookies["CartProducts"];
            if(CartProductsCookie != null)
            {
                model.CartProductIDs = CartProductsCookie.Value.Split('-').Select(x=>int.Parse(x)).ToList();
                model.CartProducts = ProductsService.Instance.GetCartProducts(model.CartProductIDs);    
            }
            return View(model);
        }
    }
}