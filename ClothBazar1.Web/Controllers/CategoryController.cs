﻿using ClothBazar.Entities;
using ClothBazar.Services;
using ClothBazar1.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar1.Web.Controllers
{

    public class CategoryController : Controller
    {

        //CategoriesService categoryService = new CategoriesService();

             
        //GET
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CategoryTable(string search, int ? PageNo)
        {
            CategorySearchViewModel model = new CategorySearchViewModel();
            model.SearchTerm = search;

            PageNo = PageNo.HasValue ? PageNo.Value > 0 ? PageNo.Value : 1 : 1;

            var totalRecords = CategoriesService.Instance.GetCategoriesCount(search);

            model.categories = CategoriesService.Instance.GetCategories(search,PageNo.Value);

            //var categories = CategoriesService.Instance.GetCategories();


            if (model.categories != null)
            {
                model.Pager = new Pager(totalRecords, PageNo, int.Parse(ConfigurationsService.Instance.GetConfiguration("ListingPageSize").Value));
                return PartialView(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        //Create
        [HttpGet]
        public ActionResult Create()
        {
            NewCategoryViewModel model = new NewCategoryViewModel();
            return PartialView(model);
        }


        [HttpPost]
        public ActionResult Create(NewCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newCategory = new Category();

                newCategory.Name = model.Name;
                newCategory.Description = model.Description;
                newCategory.ImageURL = model.ImageURL;
                newCategory.IsFeatured = model.isFeatured;


                CategoriesService.Instance.SaveCategory(newCategory);
                return RedirectToAction("CategoryTable");
            }
            else
            {
                return new HttpStatusCodeResult(500);
            }
        }


        //Edit
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditCategoryViewModel model = new EditCategoryViewModel();

            var category = CategoriesService.Instance.GetCategory(ID);

            model.ID = category.ID;
            model.Name = category.Name;
            model.Description = category.Description;
            model.ImageURL = category.ImageURL;
            model.isFeatured = category.IsFeatured;


            return PartialView(model);
        }


        [HttpPost]
        public ActionResult Edit(EditCategoryViewModel model)
        {
            var existingCategory = CategoriesService.Instance.GetCategory(model.ID);


            existingCategory.Name = model.Name;
            existingCategory.Description = model.Description;
            existingCategory.IsFeatured = model.isFeatured;
            existingCategory.ImageURL = model.ImageURL;

            CategoriesService.Instance.UpdateCategory(existingCategory);
            return RedirectToAction("CategoryTable");
        }



        [HttpPost]
        public ActionResult Delete(int ID)
        {
            CategoriesService.Instance.DeleteCategory(ID);
            return RedirectToAction("CategoryTable");
        }
    }
}