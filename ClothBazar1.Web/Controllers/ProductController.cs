﻿using ClothBazar.Entities;
using ClothBazar.Services;
using ClothBazar1.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar1.Web.Controllers
{
    public class ProductController : Controller
    {

        //ProductsService productsService = new ProductsService();

        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProductTable(string search, int ? PageNo)
        {
            ProductSearchViewModel model = new ProductSearchViewModel();
            model.SearchTerm = search;


            PageNo = PageNo.HasValue ? PageNo.Value > 0 ? PageNo.Value : 1 : 1;

            var totalRecords = ProductsService.Instance.GetProductsCount(search);

            model.Products = ProductsService.Instance.GetProducts(search, PageNo.Value);

            //var categories = CategoriesService.Instance.GetCategories();


            if (model.Products != null)
            {
                model.Pager = new Pager(totalRecords, PageNo, int.Parse(ConfigurationsService.Instance.GetConfiguration("ListingPageSize").Value));
                return PartialView(model);
            }
            else
            {
                return HttpNotFound();
            }
        }


        //Create
        [HttpGet]
        public ActionResult Create()
        {
            NewProductViewModel model = new NewProductViewModel();
            //CategoriesService categoriesService = new CategoriesService();
            model.AvailableCategories = CategoriesService.Instance.GetAllCategories();
            return PartialView(model);
        }


        [HttpPost]
        public ActionResult Create(NewProductViewModel model)
        {
            //CategoriesService categoriesService = new CategoriesService();
            var newProduct = new Product();
            newProduct.Name = model.Name;
            newProduct.Description = model.Description;
            newProduct.Price = model.Price;
            newProduct.ImageURL = model.ImageURL;
            newProduct.CreatedOn = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            //newProduct.CategoryID = model.CategoryID;
            newProduct.Category = CategoriesService.Instance.GetCategory(model.CategoryID);

            ProductsService.Instance.SaveProduct(newProduct);
            return RedirectToAction("ProductTable");
        }



        //Edit
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditProductViewModel model = new EditProductViewModel();
            var product = ProductsService.Instance.GetProduct(ID);

            model.ID = product.ID;
            model.Name = product.Name;
            model.Description = product.Description;
            model.Price = product.Price;
            model.ImageURL = product.ImageURL;

            model.CategoryID = product.Category != null ? product.Category.ID : 0;

            model.AvailableCategories = CategoriesService.Instance.GetAllCategories();

            return PartialView(model);
        }


        [HttpPost]
        public ActionResult Edit(EditProductViewModel model)
        {
            var existingProduct = ProductsService.Instance.GetProduct(model.ID);
            existingProduct.Name = model.Name;
            existingProduct.Description = model.Description;
            existingProduct.Price = model.Price;
            existingProduct.ImageURL = model.ImageURL;
            existingProduct.Category = CategoriesService.Instance.GetCategory(model.CategoryID);

            //product.CreatedOn = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            ProductsService.Instance.UpdateProduct(existingProduct);
            return RedirectToAction("ProductTable");
        }

        [HttpPost]
        public ActionResult Delete(int ID)
        {
            ProductsService.Instance.DeleteProduct(ID);
            return RedirectToAction("ProductTable");
        }
    }
}