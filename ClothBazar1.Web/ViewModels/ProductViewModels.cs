﻿using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClothBazar1.Web.ViewModels
{

    public class ProductSearchViewModel
    {
        public Pager Pager { get; set; }
        public int PageNo { get; set; }
        public string SearchTerm { get; set; }
        public List<Product> Products { get; set; }
    }

    public class NewProductViewModel
    {
        public String Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string DateTime { get; set; }
        public string ImageURL { get; set; }
        public int CategoryID { get; set; }

        public List<Category> AvailableCategories { get; set; }
    }

    public class EditProductViewModel
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string ImageURL { get; set; }
        public int CategoryID { get; set; }

        public List<Category> AvailableCategories { get; set; }
    }
}