﻿using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClothBazar1.Web.ViewModels
{
    public class CategorySearchViewModel
    {
        public List<Category> categories { get; set; }
        public string SearchTerm { get; set; }
        public Pager Pager { get; set; }
    }
    public class NewCategoryViewModel
    {
        [Required]
        [MaxLength(50), MinLength(5)]
        public String Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public string ImageURL { get; set; }
        public bool isFeatured { get; set; }

    }

    public class EditCategoryViewModel
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public string Description { get; set; }
        public string ImageURL { get; set; }
        public bool isFeatured { get; set; }

    }
}